#include "dllmain.h"

typedef HRESULT( WINAPI* oEndScene )( LPDIRECT3DDEVICE9 pDevice );
oEndScene pEndScene;

cMenuManager MenuManager;

HRESULT APIENTRY hEndScene( LPDIRECT3DDEVICE9 pDevice )
{
    Render.Device = pDevice;

    if ( !Render.init )
    {
        Render.InitText();
    }

    if ( !Render.pFont )
        Render.pFont->OnLostDevice();
    else
    {
        MenuManager.perform();

        Render.pFont->OnLostDevice();
        Render.pFont->OnResetDevice();
    }

    return pEndScene( pDevice );
}

void Hook( DWORD address )
{
    LPDIRECT3DDEVICE9 Device = ( LPDIRECT3DDEVICE9 )*(int*)( address );
    DWORD *pvTable = ( DWORD* )*(DWORD*)Device;

    pEndScene = (oEndScene)DetourFunction( (PBYTE)pvTable[ 42 ], (PBYTE)hEndScene );
}