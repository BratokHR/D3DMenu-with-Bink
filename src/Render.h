#pragma once

#define C_Text ( DT_CENTER | DT_NOCLIP )
#define L_Text ( DT_LEFT   | DT_NOCLIP )
#define R_Text ( DT_RIGHT  | DT_NOCLIP )

typedef float vec_t;
typedef vec_t vec2_t[ 2 ];
typedef vec_t vec4_t[ 4 ];

struct D3DVERTEX
{
    float x, y, z, rhw;
    DWORD color;
};

class cRender
{
public:
    IDirect3DDevice9 *Device;
    ID3DXFont *pFont;
    bool init;

    void InitText();
    void Draw_Text( float x, float y, LPSTR text, D3DCOLOR color, DWORD ST );
    void Draw_Box( float x, float y, float w, float h, D3DCOLOR Color );

    void BeginClipping( float x, float y, float w, float h );
    void EndClipping();

    void Decompress_Frame( IDirect3DDevice9 *d3d_device, HBINK bink, BINKTEXTURESET *ts );
};

extern cRender Render;