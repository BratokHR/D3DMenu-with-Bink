#include "../dllmain.h"

cMenuManager::cMenuManager()
{
    // Выставляем дефолтные настройки меню

    init = false;
    readyForRelease = false;

    menuKey = VK_F7;

    mouseX = 0;
    mouseY = 0;
    maskTitle = 30;

    distanceToX = 0;
    distanceToY = 0;
    movingMenu = false;

    activeControl = 0;
    controlState = -1;
}

void cMenuManager::perform()
{
    // Если не создали меню, то создаем
    if ( !init )
        initForms();

    drawMenu(); // рисуем меню
    handleInput(); // обрабатываем "прерывания"

    // если меню открыто, то рисуем курсор
    if ( menuOpen )
        drawCursor();
}

void cMenuManager::drawCursor()
{
    // Курсором будет красный квадрат
    Render.Draw_Box( mouseX, mouseY, 5, 5, 0xFFFF0000 );
}

void cMenuManager::drawMenu()
{
    if ( !init )
        return;

    // По-умолчанию кадр будет равен 1, всегда.
    // Если кадр равен 1, заполняем текстуры
    // Если меню открыто и кадр не framePause, то ищем его
    // Если меню закрыто, то ищем 2 кадр
    if ( ( mainForm->Bink->FrameNum == 1 ) ||
        ( menuOpen && mainForm->Bink->FrameNum != mainForm->framePause ) ||
        ( !menuOpen && mainForm->Bink->FrameNum != 2 ) )
    {
        // Переходим к следующему кадру
        Render.Decompress_Frame( Render.Device, mainForm->Bink, &mainForm->BinkTexture );
    }

    // Если кадр равен 2 (меню закрыто), то ничего не рисуем
    if ( mainForm->Bink->FrameNum == 2 )
        return;

    // Рисуем задний фон формы
    Draw_Bink_textures( Render.Device, &mainForm->BinkTexture, mainForm->w, mainForm->h, mainForm->x, mainForm->y, 1.0f, 1.0f, 1.0f, 0 );

    // Рисуем элементы
    drawControls( mainForm );
}

void cMenuManager::drawControls( cForm* menu )
{
    // Перебираем все элементы формы и рисуем их
    for ( UINT i = 0; i < menu->controlList.size(); i++ )
    {
        cControlManager* control = menu->controlList[ i ];

        switch ( control->type )
        {
        case TYPE_CHECKBOX:
            drawCheckBox( control );
            break;
        case TYPE_BUTTON:
            drawButton( control );
            break;
        }
    }
}

void cMenuManager::drawCheckBox( cControlManager* control )
{
    // Получаем смещения по х и у
    float x = mainForm->x + control->x;
    float y = mainForm->y + control->y + maskTitle;

    // Заполняем текстуры
    if ( control->Bink->FrameNum == 1 )
        Render.Decompress_Frame( Render.Device, control->Bink, &control->BinkTexture );

    if ( menuOpen )
    {
        // Если меню открыто, то цвет стремится к белому за время 2сек
        // и позиция по оси Y к 0 за 300мс
        control->anim[ 0 ].Color( 0xFFFFFFFF, 2000 );
        control->anim[ 0 ].PositionY( 0, 300 );

        // Если меню перещается, то принудительно присваиваем позицию по оси X
        // Т.е. отключаем анимацию, иначе анимируем позицию по оси X
        if ( !movingMenu )
            control->anim[ 0 ].PositionX( x, 500 );
        else
            control->anim[ 0 ].coordX = x;
    }
    else
    {
        // Если меню закрыто, то цвет за 1сек становится прозрачным
        // позиция на оси X стримится в лево на ширину элемента за 2сек
        // позиция на оси Y стримится к 100
        control->anim[ 0 ].Color( 0x00FFFFFF, 1000 );
        control->anim[ 0 ].PositionX( x - control->w, 2000 );
        control->anim[ 0 ].PositionY( 100, 2000 );
    }

    // Как и с формой
    // Если значение элемента истино и кадр не framePause, то ищем его
    // Если значение элемента ложно, то ищем 2 кадр
    if ( ( !*(bool*)control->value && control->Bink->FrameNum != 2 ) ||
        ( *(bool*)control->value && control->Bink->FrameNum != control->framePause ) )
    {
        Render.Decompress_Frame( Render.Device, control->Bink, &control->BinkTexture );
    }

    // Рисуем галку
    // Позиция на оси Y (coordY) служит интенсивностью прозрачности текстуры, 100 - не прозрачна, 0 - прозрачна
    Draw_Bink_textures( Render.Device, &control->BinkTexture, control->w, control->h, x, y, 1.0f, 1.0f, 1.0f / control->anim[ 0 ].coordY, 0 );

    // Рисуем текст с анимированным цветом
    Render.Draw_Text( control->anim[ 0 ].coordX + control->w + 5, y, (char *)control->text.c_str(), control->anim[ 0 ].clr_frst, L_Text );
    Render.Draw_Text( control->anim[ 0 ].coordX + control->w + 5, y, (char *)control->text.c_str(), control->anim[ 0 ].clr_scnd, L_Text );
}

void cMenuManager::drawButton( cControlManager* control )
{
    // см. drawCheckBox
    float x = mainForm->x + control->x;
    float y = mainForm->y + control->y + maskTitle;

    // Если кадр не равен 1, то ищем его
    // При нажатии на кнопку, происходит сдвиг кадра
    if ( control->Bink->FrameNum != 1 )
        Render.Decompress_Frame( Render.Device, control->Bink, &control->BinkTexture );

    // Видеоэффект заполнения кнопки другим цветом это увеличивающийся круг
    // для того чтобы он не выходил за рамки кнопки, используется SCISSOR
    // Путь: https://stackoverflow.com/questions/14629910/simple-clipping-in-directx-9#answer-14884804
    Render.BeginClipping( x, y, control->w, control->h );

    Render.Draw_Box( x, y, control->w, control->h, control->anim[ 0 ].clr_frst );
    Render.Draw_Box( x, y, control->w, control->h, control->anim[ 0 ].clr_scnd );

    if ( menuOpen )
    {
        control->anim[ 0 ].Color( 0xFF0D9E67, 2000 );
        control->anim[ 1 ].Color( 0xFFFFFFFF, 2000 );

        Draw_Bink_textures( Render.Device, &control->BinkTexture, control->w * 2, control->w * 2, control->mx - control->w, control->my - control->w, 1.0f, 1.0f, 1.0f, 0 );
    }
    else
    {
        control->anim[ 0 ].Color( 0x000D9E67, 1000 );
        control->anim[ 1 ].Color( 0x00FFFFFF, 1000 );
    }

    Render.Draw_Text( x + control->w / 2, y + control->h / 2 - 10, (char*)control->text.c_str(), control->anim[ 1 ].clr_frst, C_Text );
    Render.Draw_Text( x + control->w / 2, y + control->h / 2 - 10, (char*)control->text.c_str(), control->anim[ 1 ].clr_scnd, C_Text );

    Render.EndClipping();
}

void cMenuManager::handleInput()
{
    if ( !init )
        return;

    // Если видеоэффект остановлен и нажата клавиша открытия меню
    if ( ( mainForm->Bink->FrameNum == mainForm->framePause || mainForm->Bink->FrameNum == 2 ) && ( GetAsyncKeyState( menuKey ) & 1 ) )
    {
        if ( menuOpen )
        {
            // закрываем меню и сбрасываем настройки
            menuOpen = false;
            resetInput();
        }
        else
            menuOpen = true;
    }

    // Если меню закрыто, не отслеживаем ЛКМ
    if ( !menuOpen )
        return;

    getMousePosition(); // получаем позиции курсора
    getNewActiveControl(); // получаем активный элемент

    if ( GetAsyncKeyState( VK_LBUTTON ) & 0x8000 )
    {
        // Перемещаем форму
        doMovement();

        // Если форма перемещается
        if ( movingMenu = isMovingMenu() )
            return;

        // Если нет активного элемента
        if ( !activeControl )
            return;

        // Если состояние элемента стоит "наведение", то переводим на "нажатие"
        // и даем понять, что элемент выбран
        if ( controlState == STATE_HOVER )
        {
            this->controlState = STATE_PRESSED;
            this->readyForRelease = true;
            return;
        }

        keybd_event( VK_LBUTTON, 0, KEYEVENTF_KEYUP, 0 );
    }
    else
    {
        // Если элемент выбра и курсор находится в нем
        if ( readyForRelease && controlIsInRange( activeControl ) )
        {
            cControlManager *control = activeControl;

            // Если элемент это чекбокс и видеоэффект не проигрывается
            if ( control->type == TYPE_CHECKBOX && ( control->Bink->FrameNum == 2 || control->Bink->FrameNum == control->framePause ) )
            {
                *(bool*)control->value ^= 1;
            }
            // Если элемент это кнопка и видеоэффект не проигрывается
            else if ( control->type == TYPE_BUTTON && control->Bink->FrameNum == 1 )
            {
                // Сдвигаем текущий кадр
                Render.Decompress_Frame( Render.Device, control->Bink, &control->BinkTexture );

                // Запоминаем координаты курсора
                control->mx = mouseX;
                control->my = mouseY;

                // Проверяем указатель на функцию
                if ( IsBadReadPtr( control->function, 4 ) )
                    return;

                // Вызываем функцию, передаем также в качестве параметра - элемент
                if ( control->function )
                {
                    DWORD address = (DWORD)control->function;

                    __asm
                    {
                        mov     eax, control;
                        push    eax;
                        call    address;
                        add     esp, 4
                    }
                }
            }
        }

        this->resetInput();
    }
}

void cMenuManager::resetInput()
{
    // Сбрасываем настройки

    movingMenu = false;
    readyForRelease = false;
}

void cMenuManager::getMousePosition()
{
    // Получаем координаты курсора

    POINT CursorPos;

    GetCursorPos( &CursorPos );
    ScreenToClient( GetForegroundWindow(), &CursorPos );

    mouseX = (float)CursorPos.x;
    mouseY = (float)CursorPos.y;
}


void cMenuManager::getNewActiveControl()
{
    // Получаем элемент под курсором
    cControlManager *newControl = getControlAtMousePosition();

    // Если его нет, переводим состояние в "нормально" и выходим
    if ( !newControl )
    {
        controlState = STATE_NORMAL;
        activeControl = NULL;
        return;
    }

    // Иначе запоминаем элемент и говорим что мы "навелись"
    activeControl = newControl;
    controlState = STATE_HOVER;
}

cControlManager *cMenuManager::getControlAtMousePosition()
{
    // Получаем элемент под курсором

    // Если мы нажали на ЛКМ и элемент уже выбран
    if ( controlState == STATE_PRESSED )
        return activeControl;

    for ( UINT i = 0; i < mainForm->controlList.size(); i++ )
    {
        if ( controlIsInRange( mainForm->controlList[ i ] ) )
            return mainForm->controlList[ i ];
    }

    return 0;
}

bool cMenuManager::controlIsInRange( cControlManager *control )
{
    // Если курсор в рамках элемента
    if ( rangeMouse( mainForm->x + control->x, mainForm->x + control->x + control->w, mainForm->y + control->y + maskTitle, mainForm->y + control->y + control->h + maskTitle ) )
        return true;

    return false;
}

void cMenuManager::doMovement()
{
    // Перетаскивание меню

    if ( this->movingMenu )
    {
        mainForm->x = mouseX - distanceToX;
        mainForm->y = mouseY - distanceToY;
        return;
    }

    distanceToX = mouseX - mainForm->x;
    distanceToY = mouseY - mainForm->y;
}

bool cMenuManager::isMovingMenu()
{
    if ( this->movingMenu )
        return true;

    return isOverTitlebar();
}

bool cMenuManager::isOverTitlebar()
{
    if ( this->rangeMouse( mainForm->x, mainForm->y + mainForm->w, mainForm->y, mainForm->y + maskTitle ) )
        return true;

    return false;
}

bool cMenuManager::rangeMouse( float x1, float x2, float y1, float y2 )
{
    if ( this->mouseX > x1 && this->mouseX < x2 && this->mouseY > y1 && this->mouseY < y2 )
        return true;

    return false;
}