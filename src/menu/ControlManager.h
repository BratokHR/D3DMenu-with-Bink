#pragma once

enum
{
    TYPE_BUTTON,
    TYPE_CHECKBOX,

    STATE_NORMAL = 0,
    STATE_HOVER,
    STATE_PRESSED,
};

class cControlManager
{
public:
    std::string text;
    float x, y, w, h;
    int type; // тип
    void *value; // значение

    CAnim anim[ 2 ]; // анимации

    BINK *Bink;
    BINKTEXTURESET BinkTexture;
    int framePause; // кадр на котором нужно остановиться

    // Button
    void *function; // функция вызываемая при нажатии кнопки
    float mx, my; // координаты курсора в кнопке (при нажатии)
};

class cButton : public cControlManager
{
public:
    cButton( std::string text, float x, float y, float w, float h, void *func = 0 )
    {
        this->text = text;
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;

        this->type = TYPE_BUTTON;
        this->function = func;

        framePause = 0;
        Bink = BinkOpen( ".\\bik_videos\\bikmenu_button.bik", BINKALPHA | BINKNOFRAMEBUFFERS );
        BinkGetFrameBuffersInfo( Bink, &BinkTexture.bink_buffers );
        Create_Bink_textures( Render.Device, &BinkTexture );
    }
};

class cCheckBox : public cControlManager
{
public:
    cCheckBox( std::string text, float x, float y, void *value )
    {
        this->text = text;
        this->x = x;
        this->y = y;
        this->w = 20;
        this->h = 20;

        this->type = TYPE_CHECKBOX;
        this->value = value;

        framePause = 20;
        Bink = BinkOpen( ".\\bik_videos\\bikmenu_checkbox_20fr.bik", BINKALPHA | BINKNOFRAMEBUFFERS );
        BinkGetFrameBuffersInfo( Bink, &BinkTexture.bink_buffers );
        Create_Bink_textures( Render.Device, &BinkTexture );
    }
};

class cForm
{
public:
    std::string title;
    float x, y, w, h;

    BINK *Bink;
    BINKTEXTURESET BinkTexture;
    int framePause; // кадр на котором нужно остановиться

    std::vector<cControlManager*> controlList; // список элементов

    cForm( std::string title = "Form", float x = 0, float y = 0, float w = 640.0f, float h = 480.0f )
    {
        this->title = title;
        this->x = x;
        this->y = y;
        this->w = w;
        this->h = h;

        controlList.clear();

        framePause = 32;
        Bink = BinkOpen( ".\\bik_videos\\bikmenu_background_32fr.bik", BINKALPHA | BINKNOFRAMEBUFFERS );
        BinkGetFrameBuffersInfo( Bink, &BinkTexture.bink_buffers );
        Create_Bink_textures( Render.Device, &BinkTexture );
    }

    void addControl( cControlManager *control )
    {
        controlList.push_back( control );
    }
};

class cMenuManager
{
public:
    cMenuManager(); // дефолтные настройки меню

    bool menuOpen; // открыта ли меню
    short menuKey; // клавиша открытия меню
    bool readyForRelease; // отвечает за нажатие клавиши

    cControlManager *activeControl; // активный элемент (на который наведен курсор мыши)
    int controlState; // состояние элемента

    float mouseX; // координаты
    float mouseY; // мыши
    float maskTitle; // ширина заголовка, для перемещения формы

    // Перемещение формы:
    float distanceToX; 
    float distanceToY;
    bool movingMenu; // форма перемещается
    void doMovement();
    bool isMovingMenu();
    bool isOverTitlebar(); // курсор в заголовке
    //

    bool rangeMouse( float x1, float x2, float y1, float y2 );

    bool init;
    void initForms(); // инициализация формы
    void perform(); // функция запускающая 2 "потока"

    cForm *mainForm; // главная форма

    void handleInput();
    void resetInput(); // сброс настроек: состояние активного элемента, перемещение формы
    void getMousePosition(); // получение позиции мыши
    void getNewActiveControl(); // получение активного элемента
    cControlManager *getControlAtMousePosition(); // получение элемента под курсором мыши
    bool controlIsInRange( cControlManager *control ); // курсор находится в рамках элемента

    void drawMenu(); // отрисовка меню
    void drawCursor(); // отрисовка курсора
    void drawControls( cForm* menu ); // отрисовка элементов формы

    void drawCheckBox( cControlManager* control );
    void drawButton( cControlManager* control );
};