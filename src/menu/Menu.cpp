#include "../dllmain.h"

static bool wallhack = false, cheaton = true, bratokhr = true, radar = false;

void btn_click( cControlManager *control )
{
    MessageBox( 0, control->text.c_str(), control->text.c_str(), 0 );
}

void cMenuManager::initForms()
{
    BinkSoundUseDirectSound( 0 );

    if ( Create_Bink_shaders( Render.Device ) )
    {
        mainForm = new cForm( "MyForm", 200, 200, 450, 337 );

        mainForm->addControl( new cCheckBox( "Wallhack", 30, 50, &wallhack ) );
        mainForm->addControl( new cCheckBox( "CheatON", 30, 80, &cheaton ) );
        mainForm->addControl( new cCheckBox( "BratokHR", 30, 110, &bratokhr ) );
        mainForm->addControl( new cCheckBox( "Radar", 30, 140, &radar ) );

        mainForm->addControl( new cButton( "Button", 230, 250, 200, 40, btn_click ) );
    }

    init = true;
}