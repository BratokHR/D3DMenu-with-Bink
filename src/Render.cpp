#include "dllmain.h"

void cRender::InitText()
{
    D3DXCreateFont( Device, 18, 0, FW_BOLD, 1, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Verdana", &pFont );

    init = true;
}

void cRender::Draw_Text( float x, float y, LPSTR text, D3DCOLOR color, DWORD ST )
{
    RECT rect = { (long)x, (long)y, (long)x, (long)y };

    pFont->DrawTextA( NULL, text, -1, &rect, ST, color );
}

void cRender::Draw_Box( float x, float y, float w, float h, D3DCOLOR Color )
{
    D3DVERTEX vertices[ 4 ] = { { x, y, 0, 1.0f, Color },{ x + w, y, 0, 1.0f, Color },{ x, y + h, 0, 1.0f, Color },{ x + w, y + h, 0, 1.0f, Color } };
    
    IDirect3DBaseTexture9* pTexture = NULL;
    DWORD dwFVF;

    Device->GetTexture( 0, &pTexture );
    Device->GetFVF( &dwFVF );

    Device->SetTexture( 0, NULL );
    Device->SetFVF( D3DFVF_XYZRHW | D3DFVF_DIFFUSE );
    
    Device->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, vertices, sizeof( D3DVERTEX ) );

    Device->SetTexture( 0, pTexture );
    Device->SetFVF( dwFVF );
}

void cRender::BeginClipping( float x, float y, float w, float h )
{
    RECT rect = { x, y, x + w, y + h };

    Device->SetRenderState( D3DRS_SCISSORTESTENABLE, TRUE );
    Device->SetScissorRect( &rect );
}

void cRender::EndClipping()
{
    Device->SetRenderState( D3DRS_SCISSORTESTENABLE, FALSE );
}

void cRender::Decompress_Frame( IDirect3DDevice9 *d3d_device, HBINK bink, BINKTEXTURESET *ts )
{
    Lock_Bink_textures( ts );

    BinkRegisterFrameBuffers( bink, &ts->bink_buffers );
    BinkDoFrame( bink );

    Unlock_Bink_textures( d3d_device, ts, bink );
    BinkNextFrame( bink );
}

cRender Render;