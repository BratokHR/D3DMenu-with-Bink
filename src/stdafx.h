#pragma once

#include <Windows.h>
#include <process.h>
#include <string>
#include <vector>
#include <detours.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <bink.h>

#pragma comment ( lib, "d3d9.lib" )
#pragma comment ( lib, "d3dx9.lib" )
#pragma comment ( lib, "binkw32.lib" )