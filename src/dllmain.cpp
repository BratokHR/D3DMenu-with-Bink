#include "dllmain.h"

void mymain( void *param )
{
    Hook( 0x453034 );
}

bool __stdcall DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
    DisableThreadLibraryCalls( hinstDLL );

    if ( fdwReason == DLL_PROCESS_ATTACH )
    {
        _beginthread( mymain, 0, 0 );
    }

    return true;
}