#include "Animation.h"

CAnim::CAnim()
{
    binit = false;
    coordX = -1;
    coordY = -1;
}

void CAnim::init( DWORD color )
{
    syncTime_color = syncTime_positionX = syncTime_positionY = GetTickCount();

    clr_strt = clr_end = clr_frst = clr_scnd = color;
    alpha_in = 0;
    alpha_out = GetAlpha( clr_strt );
    binit = true;
}

DWORD CAnim::replace_alpha( DWORD color, float alpha )
{
    return ( (DWORD)( ( ( (int)alpha & 0xff ) << 24 ) | ( ( GetBValue( color ) & 0xff ) << 16 ) | ( ( GetGValue( color ) & 0xff ) << 8 ) | ( GetRValue( color ) & 0xff ) ) );
}

float CAnim::GetAlpha( DWORD color )
{
    return (float)( LOBYTE( ( color ) >> 24 ) );
}

void CAnim::Color( DWORD color, DWORD time )
{
    if ( !binit )
        init( color );

    DWORD _time = GetTickCount();

    if ( _time <= this->syncTime_color )
        return;

    this->syncTime_color = _time;

    time = ( ( time * 5 ) / 156 ) / 2;

    if ( color != clr_end && !reset )
    {
        clr_strt = clr_end;
        clr_end = color;
        alpha_in = 0;
        alpha_out = GetAlpha( clr_strt );
        reset = true;
    }

    float alpha = GetAlpha( clr_end );

    alpha_in += alpha / time;

    if ( alpha_in >= alpha )
    {
        alpha_in = alpha;

        alpha_out -= GetAlpha( clr_strt ) / time;

        if ( alpha_out <= 0 )
        {
            alpha_out = 0;
            reset = false; // переливание закончилось
        }
    }

    clr_frst = replace_alpha( clr_end, alpha_in );
    clr_scnd = replace_alpha( clr_strt, alpha_out );
}

void CAnim::PositionY( float coord, DWORD time )
{
    DWORD _time = GetTickCount();

    if ( _time <= this->syncTime_positionY )
        return;

    this->syncTime_positionY = _time;

    time = ( time * 5 ) / 312;

    if ( coordY == -1 )
        coordY = coord;

    coordY += ( coord - coordY ) / time; // easy-out
}

void CAnim::PositionX( float coord, DWORD time )
{
    DWORD _time = GetTickCount();

    if ( _time <= this->syncTime_positionX )
        return;

    this->syncTime_positionX = _time;

    time = ( time * 5 ) / 312;

    if ( coordX == -1 )
        coordX = coord;

    coordX += ( coord - coordX ) / time; // easy-out
}