#pragma once

#include <Windows.h>

class CAnim
{
private:
    float	alpha_in;
    float	alpha_out;

    bool	reset;

    DWORD   time_color;
    DWORD   syncTime_color;
    DWORD   syncTime_positionX;
    DWORD   syncTime_positionY;

    void    init( DWORD color );
    bool    binit;

    DWORD	replace_alpha( DWORD color, float alpha );
    float	GetAlpha( DWORD color );

public:
    CAnim();

    DWORD   clr_end;
    DWORD	clr_strt;
    DWORD	clr_frst;
    DWORD	clr_scnd;

    float   coordY;
    float   coordX;

    int time;
    int tmp_time;

    void    Color( DWORD color, DWORD time );
    void    PositionY( float coord, DWORD time );
    void    PositionX( float coord, DWORD time );
};